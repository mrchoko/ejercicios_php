<!DOCTYPE html>
<html>
<head>
	<title>Prueba</title>
</head>
<body>
<?php

	class CabeceraPagina{
		private $titulo;
		private $ubicacion;
		private $fondo;
		private $fuente;
		public function __construct($tit,$ubi,$fon,$fue){
			$this->titulo=$tit;
			$this->ubicacion=$ubi;
			$this->fondo=$fon;
			$this->fuente=$fue;
		}
		public function graficar(){
			echo '<div style="font-size:40px;text-align:'.$this->ubicacion.';background:'.$this->fondo.';color:'.$this->fuente.'">';
			echo $this->titulo;
			echo '</div>';
		}
	}

	$cabecera= new CabeceraPagina('El blog del programador','center','aqua','red');
	$cabecera->graficar();

?>
</body>
</html>