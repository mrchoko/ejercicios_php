<html>
  <head>
    <title></title>
  </head>
<body>

<?php

  class Empleado{
    private $nombre;
    private $sueldo;

    public function inicializar($nom,$sue){
      $this->nombre=$nom;
      $this->sueldo=$sue;
    }

    public function imprimir(){
      echo $this->nombre;
      if($this->sueldo > 3000)
        echo ' Debe pagar impuestos';
      else
        echo ' No debe pagar impuestos';
      echo '</br>';
    }

  }

  $per1 = new Empleado();
  $per1->inicializar('Liliana Evangelista Marcial',5000);
  $per1->imprimir();

  $per2 = new Empleado();
  $per2->inicializar('Jonathan Noyola Macias',2000);
  $per2->imprimir();

?>

</body>
</html>
