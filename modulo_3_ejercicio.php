<html>
  <head>
    <title>Pruebas</title> 
  </head>

<body>

<?php 

  class Menu{

    private $enlaces=array();
    private $titulos=array();
    
    public function cargarOption($en,$tit){
    	$this->enlaces[]=$en;
    	$this->titulos[]=$tit;
    }

    public function mostrarVertical(){
    	for($f=0;$f<count($this->enlaces);$f++){
    		echo '<a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';
    		echo "</br>";
    	}
    }

    public function mostrarHorizontal(){
      for($f=0;$f<count($this->enlaces);$f++){
        echo '<a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';
        echo " ";
      }
    }    

  }

  $menu1=new Menu();
  $menu1->cargarOption('http://www.google.com','Google');
  $menu1->cargarOption('http://www.yahoo.com','Yahoo');
  $menu1->cargarOption('http://www.msn.com','MSN');
  $menu1->mostrarHorizontal();

?>

</body>

</html>